
This module allows you to display fields in every region, before or after
regular blocks. So you are able to add node related content elsewhere on
your page.

Field settings can be done for every node type.

You can restrict usage to indicated field types. Default for allowed field
types is text_long, image and file.

The module provides a new template suggestion, so you are able to theme
the output of field by node type and region.


-- INSTALLATION --

* Copy fields_in_regions module to your modules directory and enable it on the
  admin modules page.

* Set access rights for admin and view on the access control page.

* Visit the config page on Structure > Configure Fields in Regions module
  and select fields per node type to display them in a region of your choice.

* If you want to display more field types than text_long, image and file,
  then select the allowed field types on the configuration page. After
  saving the field type settings you are able to select fields of the
  allowed type to display them in the desired region.

* You can define if the fields will be displayed before or after the regular
  blocks in the region settings.

* You are able to change field settings on the content type's field display
  settings page, too.


-- THEMING --

* If you with to theme fields by region and/or node type, create a new
  template file. The file has to be named like this:
  fields--[field_name]--[region].tpl.php
  fields--[field_name]--[node_type]--[region].tpl.php


-- AUTHOR --

Sascha Silbermann
For contact: http://drupal.org/user/1624142
