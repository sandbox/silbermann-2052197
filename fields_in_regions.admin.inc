<?php

/**
 * @file
 * Settings for fields and regions
 *
 * @description
 * Select fields to display them in a region of your choice. Define by region
 * if fields will be printed before or after regular blocks.
 */

/**
 * Form function, called by drupal_get_form() in fields_in_regions_menu().
 */
function fields_in_regions_settings_form($form, &$form_state) {
  // Get default theme.
  $theme = variable_get('theme_default', 'none');

  // Get region and field settings.
  $settings = variable_get('fields_in_regions__' . $theme, FALSE);

  // Get allowed field types.
  if (isset($form_state['values']['field_type_settings__allowed_types'])) {
    $allowed_types = array_filter($form_state['values']['field_type_settings__allowed_types']);
  }
  else {
    $allowed_types = fields_in_regions_get_allowed_types();
  }

  // Get regions.
  $regions = system_region_list($theme);
  $region_options = array('_none' => '- default -');

  $form['headline_regions_settings'] = array(
    '#markup' => '<h2>' . t('Region settings') . '</h2>',
  );

  $form['display_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Region display options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Select if fields will be displayed before or after regular blocks.
  foreach ($regions as $region => $name) {
    $default_settings = FALSE;
    if (isset($settings['region_settings'][$region])) {
      $default_settings = $settings['region_settings'][$region];
    }

    $form['display_options']['region_settings__' . $region] = array(
      '#type' => 'select',
      '#title' => $name,
      '#default_value' => $default_settings,
      '#options' => array(
        'before' => t('Display fields before regular blocks'),
        'after' => t('Display fields after regular blocks'),
      ),
    );

    $region_options[$region] = $name;
  }

  // Field type settings.
  $form['headline_allowed_field_types'] = array(
    '#markup' => '<h2>' . t('Field type settings') . '</h2>',
  );

  $form['field_type_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Allowed field types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Get field type information to select allowed field types for usage.
  $field_types = field_info_field_types();
  $field_options = array();

  foreach ($field_types as $field_type_name => $field_type) {
    $field_options[$field_type_name] = $field_type['label'];
  }

  $form['field_type_settings']['field_type_settings__allowed_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select allowed field types'),
    '#default_value' => $allowed_types,
    '#options' => $field_options,
    '#ajax' => array(
      'callback' => 'fields_in_regions_settings_form_js',
      'wrapper' => 'node_type_settings',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  $form['headline_node_types'] = array(
    '#markup' => '<h2>' . t('Field settings per node type') . '</h2>',
  );

  $form['node_type_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#theme' => 'node_type_settings',
    '#prefix' => '<div id="node_type_settings">',
    '#suffix' => '</div>',
  );

  $query = db_select('field_config_instance', 'f');
  $query->condition('entity_type', 'node');
  $query->condition('f.deleted', 0);
  $query->leftJoin('node_type', 'n', 'n.type = f.bundle');
  $query->innerJoin('field_config', 'c', 'c.field_name = f.field_name');
  $query->condition('c.type', $allowed_types, 'IN');
  $query->fields('f', array('field_id', 'field_name', 'bundle'));
  $query->addField('n', 'name', 'node_type');
  $query->orderBy('n.name', 'ASC');
  $query->orderBy('f.field_name', 'ASC');

  $results = $query->execute();
  $field_group = '';

  while ($field = $results->fetchAssoc()) {
    if ($field_group != $field['node_type']) {
      $field_group = $field['node_type'];
      $form['node_type_settings'][$field_group] = array(
        '#type' => 'fieldset',
        '#title' => $field_group,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
    }

    $default_settings = FALSE;
    if (isset($settings['field_settings'][$field['bundle'] . '__' . $field['field_name']])) {
      $default_settings = $settings['field_settings'][$field['bundle'] . '__' . $field['field_name']];
    }

    // Save field display settings as module config vars.
    $form['node_type_settings'][$field_group]['field_settings__' . $field['bundle'] . '__' . $field['field_name']] = array(
      '#type' => 'select',
      '#title' => $field['field_name'],
      '#default_value' => $default_settings,
      '#options' => $region_options,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

/**
 * AJAX callback function.
 */
function fields_in_regions_settings_form_js($form, &$form_state) {
  return $form['node_type_settings'];
}

/**
 * Validate function called by fields_in_regions_settings_form().
 */
function fields_in_regions_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $allowed_types = array_filter($values['field_type_settings__allowed_types']);

  if (!$allowed_types) {
    form_set_error('field_type_settings__allowed_types', t('Select at least one allowed field type.'));
  }
}

/**
 * Submit function called by fields_in_regions_settings_form().
 */
function fields_in_regions_settings_form_submit($form, &$form_state) {
  $theme = variable_get('theme_default', 'none');

  form_state_values_clean($form_state);
  $values = $form_state['values'];

  $settings = array();

  // Get fields of allowed types.
  $query = db_select('field_config', 'f');
  $query->fields('f', array('field_name'));
  $query->condition('f.deleted', 0);

  $allowed_types = array_filter($values['field_type_settings__allowed_types']);
  $query->condition('f.type', $allowed_types, 'IN');

  $results = $query->execute();
  $allowed_fields = array();

  // Set allowed fields.
  while ($field_name = $results->fetchField()) {
    $allowed_fields[] = $field_name;
  }

  // Prepare data to save with variable_set().
  foreach ($values as $name => $value) {
    if ($value != '_none') {
      $save_settings = TRUE;

      $data = explode('__', $name);
      $settings_type = $data[0];
      array_shift($data);
      $settings_name = implode('__', $data);

      // Check for allowed fields. If allowed, save field settings.
      if ($settings_type == 'field_settings' && !in_array($data[1], $allowed_fields)) {
        $save_settings = FALSE;
      }

      // If no problems occurs, save settings.
      if ($save_settings) {
        $settings[$settings_type][$settings_name] = $value;
      }
    }
  }

  variable_set('fields_in_regions__' . $theme, $settings);
}
